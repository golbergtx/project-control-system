const cors = require('cors');
const uniqid = require('uniqid');
const express = require('express');
const sessions = require('express-session');
const cookieParser = require('cookie-parser');

const routesAuth = require('./src/routes/auth');
const routesUser = require('./src/routes/user');
const routesJira = require('./src/routes/jira');
const routesTeam = require('./src/routes/team');
const routesClient = require('./src/routes/client');

const app = express();
const ONE_DAY = 1000 * 60 * 60 * 24;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('../client/dist'));

app.use(cors({
  origin: 'http://localhost:8080',
  methods: ['POST', 'PUT', 'GET', 'OPTIONS', 'HEAD'],
  credentials: true,
}));

app.use(sessions({
  resave: false,
  secret: uniqid(),
  saveUninitialized: true,
  cookie: { maxAge: ONE_DAY },
}));

app.use(cookieParser());

app
  .use('/', routesClient)
  .use('/auth', routesAuth)
  .use('/user', routesUser)
  .use('/jira', routesJira)
  .use('/team', routesTeam);

app.listen(3000, () => {
  console.log('App started');
});
