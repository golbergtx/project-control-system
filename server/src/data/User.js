class User {
  constructor(userID, options) {
    this.userID = userID;
    this.login = options.login;
    this.firstName = options.firstName;
    this.lastName = options.lastName;
    this.email = options.email;
    this.access = options.access;
  }

  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}

module.exports = User;
