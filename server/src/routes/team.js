const express = require('express');
const { ObjectID } = require('mongodb');
const TeamController = require('../controllers/Team');
const UserController = require('../controllers/User');

const router = express.Router();

router.post('/get-team', async (req, res) => {
  console.log('route:', '/get-team');

  try {
    const { userID } = req.body;

    const team = await TeamController.getTeam(userID);

    res.status(200).json(team);
  } catch (error) {
    console.log(error);

    if (error.message === 'Team not found') {
      res.status(404).end(error.message);
    } else {
      res.status(500).end(error.message);
    }
  }
});

router.post('/add-team', async (req, res) => {
  console.log('route:', '/add-team');

  try {
    const { userID, team } = req.body;

    const { _id } = await TeamController.addTeam(team);

    await UserController.updateUser(userID, { team: { $ref: 'team', $id: ObjectID(_id) } });

    res.status(200).end('Team created');
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.put('/update-team', async (req, res) => {
  console.log('route:', '/update-team');

  try {
    const { userID, team } = req.body;

    const { _id } = await TeamController.getTeam(userID);

    await TeamController.updateTeam(_id, team);

    res.status(200).end('Team updated');
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

module.exports = router;
