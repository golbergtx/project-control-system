const express = require('express');
const UserController = require('../controllers/User');
const User = require('../data/User');

const router = express.Router();

router.post('/login', async (req, res) => {
  console.log('route:', '/login');

  try {
    const user = await UserController.login({ ...req.body, access: 'user' });
    const { session } = req;

    session.user = new User(user._id, user);
    session.save();

    res.status(200).end('User logged');
  } catch (error) {
    console.log(error);

    if (error.message === 'User not found') {
      res.status(401).end(error.message);
    } else {
      res.status(500).end(error.message);
    }
  }
});

router.post('/registration', async (req, res) => {
  console.log('route:', '/registration');

  try {
    const user = await UserController.addUser({ ...req.body, access: 'user', team: null });
    const { session } = req;

    session.user = new User(user._id, user);
    session.save();

    res.status(200).end('User created');
  } catch (error) {
    console.log(error);

    if (error.message === 'User exist') {
      res.status(409).end(error.message);
    } else {
      res.status(500).end(error.message);
    }
  }
});

router.put('/update', async (req, res) => {
  console.log('route:', '/update');

  try {
    const { id, user } = req.body;
    await UserController.updateUser(id, user);

    res.status(200).end('User updated');
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.put('/update-password', async (req, res) => {
  console.log('route:', '/update-password');

  try {
    await UserController.updateUserPassword(req.body);

    res.status(200).end('User password updated');
  } catch (error) {
    console.log(error);

    if (error.message === 'Password incorrect') {
      res.status(403).end(error.message);
    } else {
      res.status(500).end(error.message);
    }
  }
});

router.get('/get-users', async (req, res) => {
  console.log('route:', '/get-users');

  try {
    const users = await UserController.getUsers();

    res.status(200).json(users);
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.get('/access-types', async (req, res) => {
  console.log('route:', '/access-types');

  try {
    const accessTypes = await UserController.getAccessTypes();

    res.status(200).json(accessTypes);
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.put('/update-users-roles', async (req, res) => {
  console.log('route:', '/update-users-roles');

  try {
    await UserController.updateUsersRoles(req.body);

    res.status(200).end('Users updated');
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

module.exports = router;
