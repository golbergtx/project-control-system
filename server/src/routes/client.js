const path = require('path');
const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  console.log('route:', '/');

  res.sendFile(path.resolve(__dirname, '../../../client/dist/index.html'));
});

module.exports = router;
