const express = require('express');

const router = express.Router();

router.get('/get-session', async (req, res) => {
  console.log('route:', '/get-session');

  if (req.session.user) {
    res.status(200).json(req.session.user);
  } else {
    res.status(401).json({});
  }
});

router.put('/update-session', async (req, res) => {
  console.log('route:', '/update-session');

  Object.keys(req.body).forEach((key) => {
    req.session.user[key] = req.body[key];
  });

  res.status(200).json(req.session.user);
});

router.post('/end-session', async (req, res) => {
  console.log('route:', '/end-session');

  req.session.destroy();
  res.status(200).json({});
});

module.exports = router;
