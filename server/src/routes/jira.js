const express = require('express');
const JiraController = require('../controllers/Jira');

const router = express.Router();

router.post('/get-finished-projects', async (req, res) => {
  console.log('route:', '/get-finished-projects');

  try {
    const { startDate, endDate, subTasksLabels } = req.body;
    const projects = await JiraController.getFinishedProjects(startDate, endDate, subTasksLabels);

    res.status(200).json(projects);
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.post('/get-project', async (req, res) => {
  console.log('route:', '/get-project');

  try {
    const { key, subTasksLabels } = req.body;
    const projects = await JiraController.getProject(key, subTasksLabels);

    res.status(200).json(projects);
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.post('/get-issue', async (req, res) => {
  console.log('route:', '/get-issue');

  try {
    const { key } = req.body;
    const issue = await JiraController.getIssue(key);

    res.status(200).json(issue);
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

router.post('/get-issue-bugs-keys', async (req, res) => {
  console.log('route:', '/get-issue-bugs-keys');

  try {
    const { key } = req.body;
    const bugs = await JiraController.getIssueBugsKeys(key);

    res.status(200).json(bugs);
  } catch (error) {
    console.log(error);

    res.status(500).end(error.message);
  }
});

module.exports = router;
