const { MongoClient } = require('mongodb');
const dataBaseConfig = require('../configs/db-config.json');

const options = {
  url: dataBaseConfig.url,
  dataBaseName: dataBaseConfig.dataBaseName,
  useUnifiedTopology: true,
  useNewUrlParser: true,
};

class MongoConnector {
  static getDataBase() {
    const mongoClient = new MongoClient(options.url, options);

    return new Promise((resolve, reject) => {
      mongoClient.connect((error) => {
        if (error) {
          reject(error);
        }
        resolve({
          dataBase: mongoClient.db(options.dataBaseName),
          mongoClient,
        });
      });
    });
  }
}

module.exports = MongoConnector;
