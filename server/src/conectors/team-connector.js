const { ObjectID } = require('mongodb');

class TeamConnector {
  static getTeam(dataBase, filter) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('teams');
      collection.find(filter).toArray((error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }

  static addTeam(dataBase, team) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('teams');
      collection.insertOne(team, (error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }

  static updateTeam(dataBase, id, team) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('teams');
      collection.updateOne({ _id: ObjectID(id) }, { $set: team }, (error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }
}

module.exports = TeamConnector;
