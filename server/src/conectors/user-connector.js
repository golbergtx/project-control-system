const { ObjectID } = require('mongodb');

class UserConnector {
  static getUser(dataBase, filter) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('users');
      collection.find(filter).toArray((error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }

  static getUsers(dataBase) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('users');
      collection.find({}).toArray((error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }

  static addUser(dataBase, user) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('users');
      collection.insertOne(user, (error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }

  static updateUser(dataBase, id, user) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('users');
      collection.updateOne({ _id: ObjectID(id) }, { $set: user }, (error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }

  static getAccessTypes(dataBase) {
    return new Promise((resolve, reject) => {
      const collection = dataBase.collection('access-types');
      collection.find({}).toArray((error, docs) => {
        if (error) reject(error);
        resolve(docs);
      });
    });
  }
}

module.exports = UserConnector;
