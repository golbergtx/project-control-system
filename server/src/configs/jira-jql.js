const config = require('./jira-config.json');

function getJQL(startDate, endDate) {
  return [
    `project=${config.projectsFilter.key}`,
    `created >= ${startDate}`,
    `updated <= ${endDate}`,
    `status changed TO Done BEFORE ${endDate}`,
    `assignee in ("${config.projectsFilter.assignee}")`,
    `issuetype = ${config.projectsFilter.issueType}`,
    'status = Done',
  ].join(' AND ');
}

module.exports = getJQL;
