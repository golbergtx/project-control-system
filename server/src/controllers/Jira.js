const JiraConnector = require('jira-connector');
const getJQL = require('../configs/jira-jql');
const jiraConfig = require('../configs/jira-config.json');

const jira = new JiraConnector(jiraConfig.connector);

class Jira {
  static async getFinishedProjects(startDate, endDate, subTasksLabels) {
    const { issues } = await jira.search.search({
      jql: getJQL(startDate, endDate),
      expand: ['changelog'],
      maxResults: 1000,
    });

    return issues.map((issue) => ({
      id: issue.id,
      key: issue.key,
      self: issue.self,
      summary: issue.fields.summary,
      duedate: issue.fields.duedate,
      status: issue.fields.status.name,
      priority: issue.fields.priority.name,
      issueLinks: issue.fields.issuelinks
        .map((item) => ({
          key: item.outwardIssue.key,
          summary: item.outwardIssue.fields.summary,
        }))
        .filter((item) => subTasksLabels.some((label) => item.summary.includes(label))),
    }));
  }

  static async getProject(key, subTasksLabels) {
    const issue = await jira.issue.getIssue({ issueKey: key });

    return {
      id: issue.id,
      key: issue.key,
      self: issue.self,
      summary: issue.fields.summary,
      duedate: issue.fields.duedate,
      status: issue.fields.status.name,
      priority: issue.fields.priority.name,
      issueLinks: issue.fields.issuelinks
        .map((item) => ({
          key: item.outwardIssue.key,
          summary: item.outwardIssue.fields.summary,
        }))
        .filter((item) => subTasksLabels.some((label) => item.summary.includes(label))),
    };
  }

  static async getIssue(key) {
    const issue = await jira.issue.getIssue({ issueKey: key });

    return {
      id: issue.id,
      key: issue.key,
      self: issue.self,
      summary: issue.fields.summary,
      duedate: issue.fields.duedate,
      status: issue.fields.status.name,
      priority: issue.fields.priority.name,
      logged: issue.fields.aggregatetimespent,
      estimation: issue.fields.aggregatetimeoriginalestimate,
    };
  }

  static async getIssueBugsKeys(key) {
    const bugs = [];
    const issue = await jira.issue.getIssue({ issueKey: key });

    // eslint-disable-next-line no-restricted-syntax
    for await (const task of issue.fields.subtasks) {
      const subIssue = await jira.issue.getIssue({ issueKey: task.key });
      bugs.push(...subIssue.fields.issuelinks.map((item) => item.inwardIssue.key));
    }

    return bugs;
  }
}

module.exports = Jira;
