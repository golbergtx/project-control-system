const { ObjectID } = require('mongodb');
const MongoConnector = require('../conectors/mongo-connector');
const TeamConnector = require('../conectors/team-connector');
const UserConnector = require('../conectors/user-connector');

class Team {
  static async getTeam(userID) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const user = await UserConnector.getUser(dataBase, { _id: ObjectID(userID) });

      if (!user[0].team) throw Error('Team not found');

      const team = await TeamConnector.getTeam(dataBase, { _id: ObjectID(user[0].team.oid) });

      if (!team[0]) throw Error('Team not found');

      return team[0];
    } finally {
      mongoClient.close();
    }
  }

  static async addTeam(team) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const result = await TeamConnector.addTeam(dataBase, team);

      return result.ops[0];
    } finally {
      mongoClient.close();
    }
  }

  static async updateTeam(id, team) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      await TeamConnector.updateTeam(dataBase, id, team);
    } finally {
      mongoClient.close();
    }
  }
}

module.exports = Team;
