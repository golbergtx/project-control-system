const { ObjectID } = require('mongodb');
const MongoConnector = require('../conectors/mongo-connector');
const UserConnector = require('../conectors/user-connector');

class User {
  static async login(options) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const user = await UserConnector.getUser(dataBase, { login: options.login });

      if (!user[0]) throw Error('User not found');
      if (options.password !== user[0].password) throw Error('User not found');

      return user[0];
    } finally {
      mongoClient.close();
    }
  }

  static async addUser(options) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const user = await UserConnector.getUser(dataBase, { login: options.login });

      if (user[0]) throw Error('User exist');

      const newUser = await UserConnector.addUser(dataBase, options);

      return newUser.ops[0];
    } finally {
      mongoClient.close();
    }
  }

  static async updateUser(id, user) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      await UserConnector.updateUser(dataBase, id, user);
    } finally {
      mongoClient.close();
    }
  }

  static async updateUserPassword(options) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const user = await UserConnector.getUser(dataBase, { _id: ObjectID(options.id) });

      if (user[0].password !== options.oldPassword) throw Error('Password incorrect');

      await UserConnector.updateUser(dataBase, options.id, {
        password: options.newPassword,
      });
    } finally {
      mongoClient.close();
    }
  }

  static async getUsers() {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const result = await UserConnector.getUsers(dataBase);

      return result.map((item) => ({
        _id: item._id,
        login: item.login,
        firstName: item.firstName,
        lastName: item.lastName,
        access: item.access,
      }));
    } finally {
      mongoClient.close();
    }
  }

  static async getAccessTypes() {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      const result = await UserConnector.getAccessTypes(dataBase);

      return result.map((item) => item.type);
    } finally {
      mongoClient.close();
    }
  }

  static async updateUsersRoles(options) {
    const { dataBase, mongoClient } = await MongoConnector.getDataBase();

    try {
      await Promise.all(
        options.map((user) => UserConnector.updateUser(dataBase, user.id, {
          access: user.access,
        })),
      );
    } finally {
      mongoClient.close();
    }
  }
}

module.exports = User;
