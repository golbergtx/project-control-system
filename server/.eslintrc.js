module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    'airbnb-base',
  ],
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'import/prefer-default-export': 0,
    'class-methods-use-this': 0,
    'import/first': 0,
    'no-unused-vars': 0,
    'vue/no-v-html': 0,
    'no-use-before-define': 0,
    'linebreak-style': 0,
    'no-underscore-dangle': 0,
    'no-param-reassign': [
      'warn',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'state',
          'acc',
          'e',
        ],
      },
    ],
    'max-len': [
      'error',
      {
        code: 150,
      },
    ],
  },
};
