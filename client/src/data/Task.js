export default class Task {
  constructor(key, link, fields) {
    this.key = key;
    this.link = link;
    this.status = fields.status;
    this.summary = fields.summary;
    this.duedate = fields.duedate;
    this.priority = fields.priority;
    this.logged = fields.logged || 0;
    this.estimation = fields.estimation || 0;
  }
}
