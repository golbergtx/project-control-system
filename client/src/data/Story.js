import Task from './Task';

export default class Story extends Task {
  constructor(key, link, fields) {
    super(key, link, fields);
    this.subTasks = [];
    this.bugsKeys = [];
  }

  setSubTasks(subTasks) {
    this.subTasks = subTasks;
  }

  setBugsKeys(bugsKeys) {
    this.bugsKeys = bugsKeys;
  }

  getSubTasksEstimations() {
    return this.subTasks.map((task) => task.estimation);
  }

  getSubTasksLogs() {
    return this.subTasks.map((task) => task.logged);
  }

  getSumSubTasksEstimationTime() {
    const sumTime = this.subTasks.reduce((time, task) => time + task.estimation, this.estimation);
    return getHoursTime(sumTime);
  }

  getSumSubTasksLoggedTime() {
    const sumTime = this.subTasks.reduce((time, task) => time + task.logged, this.logged);
    return getHoursTime(sumTime);
  }

  getOverLog() {
    return `${parseInt(this.getSumSubTasksLoggedTime()) - parseInt(this.getSumSubTasksEstimationTime())} год.`;
  }

  getPercentageOverLog() {
    const overLog = parseInt(this.getOverLog());
    const sumEstimationTime = parseInt(this.getSumSubTasksEstimationTime());

    if (overLog <= 0 || sumEstimationTime === 0) return '0 %';

    return `${Math.round(overLog / sumEstimationTime * 100)} %`;
  }

  getSubTaskTest() {
    return this.subTasks.find((task) => task.summary.includes('Test'));
  }

  getRatioAmountBugsToEstimationTime() {
    return Math.round(this.bugsKeys.length / parseInt(this.getSumSubTasksEstimationTime()) * 100) / 100 || 0;
  }
}

function getHoursTime(time) {
  return `${Math.round(time * 10 / 60 / 60) / 10} год.`;
}
