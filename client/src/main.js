import Vue from 'vue';
import Router from 'vue-router';

import ECharts from 'vue-echarts';
import { use } from 'echarts/core';

import {
  CanvasRenderer,
} from 'echarts/renderers';
import {
  PieChart,
  BarChart,
  RadarChart,
  GaugeChart,
} from 'echarts/charts';
import {
  GridComponent,
  TitleComponent,
  LegendComponent,
  TooltipComponent,
  ToolboxComponent,
} from 'echarts/components';

import App from './App.vue';
import routes from './routes/index';
import vuetify from './plugins/vuetify';
import store from './store/index';
import axios from './api/backend';

use([
  BarChart,
  PieChart,
  GaugeChart,
  RadarChart,
  GridComponent,
  TitleComponent,
  CanvasRenderer,
  LegendComponent,
  TooltipComponent,
  ToolboxComponent,
]);

Vue.component('VChart', ECharts);

Vue.config.productionTip = false;
Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes,
});

router.beforeEach(async (to, from, next) => {
  await axios
    .get('/auth/get-session', { withCredentials: true })
    .then((response) => {
      store.dispatch('common/auth', { user: response.data, authenticated: true });
    })
    .catch((error) => {
      if (error.response.status === 401) {
        store.dispatch('common/auth', { user: null, authenticated: false });
      }
    });

  if (!to.meta.guest && !store.state.common.authenticated) {
    next({ path: '/login-page' });
  } else {
    next();
  }
});

new Vue({
  store,
  router,
  vuetify,
  data: {},
  render: (h) => h(App),
}).$mount('#app');
