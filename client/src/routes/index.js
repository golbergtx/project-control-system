import loginPage from '../pages/LoginPage.vue';
import profilePage from '../pages/ProfilePage.vue';
import projectPage from '../pages/ProjectPage.vue';
import adminPanelPage from '../pages/AdminPanelPage.vue';
import registrationPage from '../pages/RegistrationPage.vue';
import projectsLogsPage from '../pages/ProjectsLogsPage.vue';
import projectsBugsPage from '../pages/ProjectsBugsPage.vue';
import projectPortfolioPage from '../pages/ProjectPortfolioPage.vue';

export default [
  {
    name: 'Login page',
    path: '/login-page',
    component: loginPage,
    meta: {
      guest: true,
    },
  },
  {
    name: 'Registration page',
    path: '/registration-page',
    component: registrationPage,
    meta: {
      guest: true,
    },
  },
  {
    name: 'Profile page',
    path: '/profile-page',
    component: profilePage,
  },
  {
    name: 'Admin panel page',
    path: '/admin-panel-page',
    component: adminPanelPage,
  },
  {
    name: 'Projects logs page',
    path: '/projects-logs-page',
    component: projectsLogsPage,
  },
  {
    name: 'Project bugs page',
    path: '/projects-bugs-page',
    component: projectsBugsPage,
  },
  {
    name: 'Project page',
    path: '/project-page',
    component: projectPage,
  },
  {
    name: 'Project portfolio page',
    path: '/projects-portfolio-page',
    component: projectPortfolioPage,
  },
  {
    path: '*',
    component: projectsLogsPage,
  },
];
