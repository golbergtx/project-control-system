export default {
  namespaced: true,
  state: {
    user: null,
    authenticated: false,
  },
  mutations: {
    set_authenticated(state, payload) {
      state.authenticated = payload;
    },
    set_user(state, payload) {
      state.user = payload;
    },
  },
  actions: {
    auth(context, payload) {
      context.commit('set_authenticated', payload.authenticated);
      context.commit('set_user', payload.user);
    },
  },
};
