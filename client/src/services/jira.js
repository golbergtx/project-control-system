import axios from '../api/backend';
import Task from '../data/Task';
import Story from '../data/Story';

export default class {
  static async getStories(searchOptions) {
    const stories = [];

    const { data: projects } = await axios.post('jira/get-finished-projects', searchOptions);

    for await (const project of projects) {
      const story = new Story(project.key, this.getIssueLink(project.self, project.key), project);

      story.setSubTasks(await Promise.all(
        project.issueLinks.map((issue) => this.getIssue(issue.key)),
      ));

      stories.push(story);
    }

    console.log('getStories', stories);
    return stories;
  }

  static async getStory(key, subTasksLabels) {
    const { data: project } = await axios.post('jira/get-project', { key, subTasksLabels });

    const story = new Story(project.key, this.getIssueLink(project.self, project.key), project);

    story.setSubTasks(await Promise.all(
      project.issueLinks.map((issue) => this.getIssue(issue.key)),
    ));

    console.log('getStory', story);
    return story;
  }

  static async getIssue(key) {
    const { data: issue } = await axios.post('jira/get-issue', { key });
    return new Task(issue.key, this.getIssueLink(issue.self, issue.key), issue);
  }

  static async getBugsKeys(key) {
    const { data: bugsKeys } = await axios.post('jira/get-issue-bugs-keys', { key });
    return bugsKeys;
  }

  static getIssueLink(self, key) {
    return `${self.replace(/rest.*/, '')}browse/${key}`;
  }
}
