module.exports = {
  lintOnSave: false,
  transpileDependencies: [
    'vuetify',
    'vue-echarts',
    'resize-detector',
  ],
};
